"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve

from newsblog import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('fetch/',views.fetch,name='fetch'),
    path('fetchmore/',views.fetchmore,name='fetchmore'),
    path('show_content/<int:id>/',views.show_content,name='show_content'),
    path('login/', views.log_in, name='login'),
    path('logout/', views.log_out, name='logout'),
    path('delete/', views.delete, name='delete'),
    path('revive/', views.revive, name='revive'),
    path('search/', views.search, name='search'),
]
