# Generated by Django 2.0.5 on 2018-09-05 12:17

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('newsblog', '0002_auto_20180905_1524'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='index',
        ),
        migrations.AddField(
            model_name='news',
            name='publish_time',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='发布时间'),
        ),
    ]
