# 此文件是对于获取、分析网页信息的方法的封装
# -*- coding: UTF-8 -*-
from urllib import request,response
from bs4 import BeautifulSoup
import sys
import chardet
import zlib
import jieba
import re
import json
from django.utils.timezone import now

from newsblog.models import News

#获得系统编码格式
sys_code = sys.getfilesystemencoding()

# 反爬虫的请求头，要求gzip格式的压缩
headers = {
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
    "Accept-Encoding": "gzip",
}


def getSoup(url):
    """ 获取bs4对象 """
    try:
        req = request.Request(url, headers=headers)
        req = request.Request(url)
        html = request.urlopen(req)
        fEncode = html.info().get('Content-Encoding')
        html = html.read()
        # 判断是否经过压缩
        if fEncode == 'gzip':
            html = zlib.decompress(html, 16 + zlib.MAX_WBITS)
    except:
        print('get html %s failed!' % url)
        return None

    if html is None:
        print('get html %s blank!' % url)
        return None

    # 中文编码解决
    encoding = chardet.detect(html)['encoding']
    if encoding is None:
        encoding = 'gb2312'
    text = html.decode(encoding, 'ignore').encode(sys_code, 'ignore')

    # 利用BeautifulSoup解析网页
    soup = BeautifulSoup(text, 'html.parser')
    return soup


def getUrlList(baseurl):
    """ 根据soup对象获得新闻链接 """
    soup = getSoup(baseurl)
    if soup is None:
        return []
    urlList = []
    links = soup.find('div',attrs={'class':'part_01 clearfix'})\
        .find_all('a')
    for link in links:
        urlList.append(link.get('href'))
    return urlList


def getUsefulWords(text):
    """分词，返回一个list对象"""
    allwords = jieba.cut_for_search(text)
    useful_words = []
    pattern = '[a-zA-Z0-9\'\"{}\\(\\)\\[\\]\\*&\.?!,:;]+'
    for word in allwords:
        if (re.match(pattern, word) == None) and len(word) > 1:
            useful_words.append(word)
    # 年月日
    date_pattern = re.compile('(\d{4})[\u5E74\,\/\.\\:-](\d{1,2})[\u6708\,\/\.\\:-](\d{1,2})[\u65E5]?')
    dates = date_pattern.findall(text)
    dates = list(set(dates))
    for date in dates:
        format = '%s-%s-%s' % (date[0], date[1], date[2])
        useful_words.append(format)

    return useful_words


def parseNews(url, idx):
    """ 获取、分析news网页，并保存到数据库中 """
    soup = getSoup(url)
    if soup is None:
        return
    body = soup.find('div',attrs={'class':'article'})
    content = ''

    # title
    title = soup.head.title.string

    # 判断新闻是否已经存在
    if len(News.objects.filter(title=title)) != 0:
        return

    # pubtime
    pubTimeTag = soup.find('meta',attrs={'property':'article:published_time'})
    if pubTimeTag:
        pubTime = pubTimeTag['content']
    else:
        pubTime = ''

    # body
    if body == None:
        return
    for tag in body.descendants:
        if tag.name in ['div','p'] and tag.string:
            content += tag.string.strip()
        if tag.name in ['div','p','br']:
            content += '\n'

    # content
    content = re.sub('\n+','\n',content)

    # 分词
    useful_words = getUsefulWords(content)

    # 转成json
    dic = {}
    dic['words'] = list(set(useful_words))
    words = json.dumps(dic,ensure_ascii=False)

    obj = News(title=title,content=content,words=words,publish_time=pubTime,
                views=0,fetch_time = now())
    obj.save()
    print('html index %d saved' % idx)