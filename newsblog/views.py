from django.shortcuts import render
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import Http404
from django.conf import settings
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import login,authenticate,logout

import threading
import json

from newsblog.models import News
from newsblog.getNews import getUrlList, parseNews, getUsefulWords

# Create your views here.


def home(request):
    "主页"
    if request.user.is_authenticated: # 管理员登录状态
        posts = News.objects.all()  # 获取全部News对象
    else:
        posts = News.objects.filter(status='y')
    paginator = Paginator(posts, settings.PAGENUM)  # 每页显示数量
    page = request.GET.get('page')  # 获取URL中page参数的值
    try:
        post_list = paginator.page(page)
    except PageNotAnInteger:
        post_list = paginator.page(1)
    except EmptyPage:
        post_list = paginator.page(paginator.num_pages)
    return render(request, 'home.html', {'post_list': post_list})


def log_in(request):
    "管理员登录 "
    username = request.GET.get('username','')
    password = request.GET.get('password','')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
    return HttpResponseRedirect("/home/")


def log_out(request):
    "管理员退出登录"
    logout(request)
    return HttpResponseRedirect("/home/")


def delete(request):
    "管理员删除新闻"
    id = request.GET.get('del_id')
    post = News.objects.filter(id=id).update(status='n')
    return HttpResponseRedirect("/home/")


def revive(request):
    "管理员恢复新闻"
    id = request.GET.get('revive_id')
    post = News.objects.filter(id=id).update(status='y')
    return HttpResponseRedirect("/home/")


def search(request):
    "搜索"
    # 构建倒排文档
    inverted_docs = {}
    for news in News.objects.all():
        dic = json.loads(news.words)
        for word in dic['words']:
            if word in inverted_docs:
                inverted_docs[word].append(news.id)
            else:
                inverted_docs[word] = [news.id]
    # 对输入内容分词
    search_text = request.GET.get('search_text')
    useful_words = getUsefulWords(search_text)
    # 检索
    candidate_dic = {}
    for word in useful_words:
        if word in inverted_docs:
            for id in inverted_docs[word]:
                if id in candidate_dic:
                    candidate_dic[id] += 1
                else:
                    candidate_dic[id] = 1

    # 按照命中次数排序
    candidate_list = sorted(candidate_dic.items(), key=lambda x: x[1], reverse=True)
    num = min(settings.MAXSEARCH, len(candidate_list))
    post_list = []
    for i in range(num):
        post = News.objects.get(id=candidate_list[i][0])
        post_list.append(post)

    return render(request, 'home.html', {'post_list': post_list})


def show_content(request, id):
    "显示新闻细节 "
    try:
        post = News.objects.get(id=str(id))
        post.viewed()  # 更新浏览次数
        next_post = post.next_news()  # 上一篇文章对象
        prev_post = post.prev_news()  # 下一篇文章对象
    except News.DoesNotExist:
        raise Http404
    return render(
        request, 'content.html',
        {
            'post': post,
            'next_post': next_post,
            'prev_post': prev_post,
        }
    )


def fetch(request):
    "重新抓取网页"

    # 删除旧数据库中的内容
    News.objects.all().delete()
    # 根据新闻门户网站index_url获得链接url
    urlList = getUrlList(settings.INDEX_URL)
    urlList = list(set(urlList))
    # 多线程爬取、解析网页
    list_num = min(len(urlList),settings.MAXNUM)
    for idx, url in enumerate(urlList[:list_num]):
        thr = threading.Thread(target=parseNews, args=(url, idx))
        thr.start()
        threading.Thread.join(thr)
    return HttpResponseRedirect("/home/")


def fetchmore(request):
    "抓取新的网页"

    # 根据新闻门户网站index_url获得链接url
    urlList = getUrlList(settings.INDEX_URL)
    urlList = list(set(urlList))
    # 多线程爬取、解析网页
    list_num = min(len(urlList), settings.MAXNUM)
    for idx, url in enumerate(urlList[:list_num]):
        thr = threading.Thread(target=parseNews, args=(url, idx))
        thr.start()
        threading.Thread.join(thr)
    return HttpResponseRedirect("/home/")