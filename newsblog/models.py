from django.db import models
from django.utils.timezone import now

# Create your models here.


class News(models.Model):
    "一条新闻的类"
    # 是否可以浏览由管理员控制
    STATUS_CHOICES = (
        ('y', '可以浏览'),
        ('n', '禁止浏览'),
    )
    title = models.CharField(verbose_name='标题', max_length=100)
    content = models.TextField(verbose_name='正文',default='')
    words = models.TextField(verbose_name='分词结果',default='')
    status = models.CharField(verbose_name='状态', max_length=1, choices=STATUS_CHOICES, default='y')
    views = models.PositiveIntegerField(verbose_name='浏览量', default=0)
    publish_time = models.CharField(verbose_name='发布时间', max_length=30)
    fetch_time = models.DateTimeField(verbose_name='爬取时间', default=now)

    # 使对象在后台显示更友好
    def __str__(self):
        return self.title

    # 更新浏览量
    def viewed(self):
        self.views += 1
        self.save(update_fields=['views'])

    # 下一篇
    def next_news(self):  # id比当前id大，状态为已发布
        return News.objects.filter(id__gt=self.id).first()

    # 上一篇
    def prev_news(self):  # id比当前id小，状态为已发布
        return News.objects.filter(id__lt=self.id).first()

    class Meta:
        ordering = ['-fetch_time']  # 按文章爬取日期降序
        verbose_name = '新闻'
        verbose_name_plural = '新闻列表'
        db_table = 'news'
        get_latest_by = 'fetch_time'
