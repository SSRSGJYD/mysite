### 基于Django的新闻搜索网站

周展平 2016013253

------

[TOC]

#### 一、运行环境

`requirements.txt` 文件中写有需要的python相关的库：

```
python==3.6.1
Django==2.1.1
pytz==2018.5
jieba==0.39
beautifulsoup4==4.6.0
chardet==3.0.4
sqlite3 (django自带)
```

推荐使用虚拟环境 `virtualenv` 管理，在其中安装python3.6.1，windows环境下将项目放在Scripts文件夹中，在项目目录打开控制台，安装必要的库：

```powershell
pip install -r requirements.txt
```

另外，确保网络连接正常。



#### 二、测试方式

+ 运行项目：

```powershell
python manage.py runserver
```

+ 在浏览器中输入地址：`http://127.0.0.1:8000` ，浏览器就会显示出网站主页，如下：

  ![homepage](F:\_MyWorkingPlace\news\screenshots\homepage.PNG)

+ 点击左侧第一个按钮 **重新爬取新闻网页** ，网站就会清空原来数据库中的新闻，同时重新从新浪新闻网站(https://news.sina.com.cn/)爬取新闻。这个过程可能需要一段时间。

+ 点击左侧第二个按钮 **爬取更多的新闻网页** ，网站会保留原来数据库中的新闻，同时从新浪新闻网站(https://news.sina.com.cn/)爬取新的一些新闻。这个过程可能需要一段时间。

+ 设定的每页显示的新闻数目是最多10个，点击页面下方的 **上一页** ，**下一页** 即可翻页，如下：

![turnpage](F:\_MyWorkingPlace\news\screenshots\turnpage.PNG)

+ 点击上图中右侧箭头即可返回页面顶端。

+ 在上方管理员登录的表单中输入用户名 `admin` 和密码 `django2web` ，即可以管理员身份登录网站。登录后的界面如下：![adminpage](F:\_MyWorkingPlace\news\screenshots\adminpage.PNG)

+ 点击红色按钮  **删除此条新闻** ，此条新闻就不可以被普通访客浏览。例如，第二条新闻 `特朗普为对抗中国...` 在主页上不可见。

+ 点击绿色按钮  **恢复此条新闻** ，此条新闻重新可以被普通访客浏览。

+ 点击最上方  **退出登录** ，回到访客模式。

+ 在主页中点击新闻的新闻标题，可以在新的标签页中访问新闻详情：

  ![content](F:\_MyWorkingPlace\news\screenshots\content.PNG)

+ 点击文章底部的 **上一篇** ，**下一篇** 即可打开其他新闻。

+ 在搜索相关新闻的文本输入框中输入搜索的内容，如 `中国` ，点击按钮 **开始搜索** ，在新的标签页中打开了一个搜索结果页面，搜索结果设置为至多10条，如下：

![search](F:\_MyWorkingPlace\news\screenshots\search.PNG)



#### 三、外部库及参考资料

+ css样式：

> Font Awesome: http://www.bootcss.com/p/font-awesome/
>
> grids-responsive-min.css: 来自Bootstrap，https://getbootstrap.com/
>
> pure-min.css：https://purecss.io/
>
> blog.css: https://github.com/leeyis/jbt_blog

+ JQuery：

> jquery-3.3.1.min.js: https://jquery.com/

+ 助教的作业说明